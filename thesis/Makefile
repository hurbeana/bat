BASENAME=thesis
DISTNAME=thesis_latex
DISTFOLDER?=$(shell pwd)
CLASS=vutinfth
VIEWER=mupdf
PDFLATEX_ARGS=-file-line-error -interaction=nonstopmode -synctex=15
DIFF_COMMIT_HASH?=$(shell git describe --abbrev=0)
DIFF_TEX_NAME?=diff_$(shell date +%d%m%Y)
DIFF_STYLE?=UNDERLINE

.PHONY: default all
default: clean compile
all: clean compile doc

doc:
	pdflatex ${CLASS}.dtx
	pdflatex ${CLASS}.dtx
	makeindex -s gglo.ist -o ${CLASS}.gls ${CLASS}.glo
	makeindex -s gind.ist -o ${CLASS}.ind ${CLASS}.idx
	pdflatex ${CLASS}.dtx
	pdflatex ${CLASS}.dtx

document-class: ${CLASS}.cls
${CLASS}.cls:
	pdflatex ${CLASS}.ins

compile: document-class
	# fix a source, has one too many curly braces when exported from mendeley
	sed -i 's/{{Sp{\\"{a}}rck Jones}, Karen}/{Sp{\\"{a}}rck Jones, Karen}/g' library.bib
	pdflatex $(PDFLATEX_ARGS) $(BASENAME)
#	makeglossaries $(BASENAME)
	pdflatex $(PDFLATEX_ARGS) $(BASENAME)
#	makeglossaries $(BASENAME)
	#bibtex $(BASENAME)
	biber $(BASENAME)
	pdflatex $(PDFLATEX_ARGS) $(BASENAME)
	pdflatex $(PDFLATEX_ARGS) $(BASENAME)

diff: ${DIFF_TEX_NAME}.pdf

${DIFF_TEX_NAME}.pdf: document-class ${DIFF_TEX_NAME}.tex
	-pdflatex $(PDFLATEX_ARGS) $(DIFF_TEX_NAME)
	-pdflatex $(PDFLATEX_ARGS) $(DIFF_TEX_NAME)
	#bibtex $(DIFF_TEX_NAME)
	biber $(DIFF_TEX_NAME)
	-pdflatex $(PDFLATEX_ARGS) $(DIFF_TEX_NAME)
	-pdflatex $(PDFLATEX_ARGS) $(DIFF_TEX_NAME)
	find . -type f  -not \( -name "$(DIFF_TEX_NAME).pdf" -o -name "*.backup" \) -name "$(DIFF_TEX_NAME)*" -delete -print
	find . -type f -name '*.aux' -delete -print
	find . -type f -name '*.log' -delete -print


${DIFF_TEX_NAME}.tex:
	cp $(BASENAME).tex /tmp/$(BASENAME).new.tex
	cp $(BASENAME).tex /tmp/$(BASENAME).tex
	git diff $(DIFF_COMMIT_HASH) -- $(BASENAME).tex > /tmp/$(DIFF_TEX_NAME).diff
	cd /tmp &&\
	patch -R < $(DIFF_TEX_NAME).diff &&\
	latexdiff -t $(DIFF_STYLE) /tmp/$(BASENAME).tex /tmp/$(BASENAME).new.tex > $@ && \
	cp $@ $(DISTFOLDER) && \
	rm $(DIFF_TEX_NAME).diff $(BASENAME).tex $(BASENAME).new.tex

view:
	$(VIEWER) $(VIEWER_OPTIONS) $(BASENAME).pdf

zip: clean compile# ${DIFF_TEX_NAME}.pdf
	zip -9 -r --exclude=*.git* $(BASENAME).zip \
		${DIFF_TEX_NAME}.pdf \
		db_mend.bib \
		build-all.bat \
		build-all.sh \
		build-thesis.bat \
		build-thesis.sh \
		graphics \
		intro.bib \
		intro.tex \
		lppl.txt \
		Makefile \
		README.txt \
		README-vutinfth.txt \
		thesis.tex \
		thesis.pdf \
		vutinfth.dtx \
		vutinfth.ins

dist: zip
	cp $(BASENAME).zip $(DISTFOLDER)/$(DISTNAME).zip

.PHONY: clean
clean:
	find . -type f  -not \( -name "${BASENAME}.tex" -o -name "*.backup" \) -name "${BASENAME}*" -delete -print
	rm -f vutinfth.cls vutinfth.pdf
	rm -f vutinfth.hd vutinfth.ind
	find . -type f -name '*.aux' -delete -print
	find . -type f -name '*.log' -delete -print
	rm -f vutinfth.glo vutinfth.gls vutinfth.idx vutinfth.ilg vutinfth.out vutinfth.toc
