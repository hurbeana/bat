# Code README

## Notes on training sizes

For the Datasets following train/test sizes have been used (0.8 ratio) for all
training and followed by testing:

- agb: train=11456, test=2865
- avv: train=30093, test=7524
- nda: train=7571,  test=1893
- pur: train=5214,  test=1304
- sww: train=3529,  test=883

All numbers are in # of sentences. These scripts have been tested on multiple
datasets, but only the agb dataset has been included in the thesis.

## Setup

Please install the Anaconda environment or use venv to run the code. Python 3.6
has been used to execute this code due to limitations of the pattern framework
which can only be executed on Python 3.6. For installation of Anaconda (or
Miniconda) please under Linux please check
[here](https://docs.anaconda.com/anaconda/install/) and Windows
[here](https://docs.anaconda.com/anaconda/install/windows/)).

This project has been run under Manjaro Linux, which is based on Arch Linux
allowing for easy installation from the Arch User Repository using a third party
tool AUR helper such as [yay](https://github.com/Jguer/yay). Miniconda has been
used due to being slimmer and not having many packages preinstalled allowing for
a faster start with `yay -S miniconda `

After Anaconda has been installed issue these commands to create a new
enviroment for the project:

```
conda create -n bat python=3.6
conda activate bat
```

This creates a new conda environment called "bat" and activates it.
To make sure that the graphics card device can be used (for BERT and CNN) it is
also needed to install the latest nvidia graphics card drivers for your system
and the latest cuda toolkit. To check which version of drivers are installed the
following commands should work and output versions:

```
nvidia-smi
nvcc --version
```

After that install all the dependecies into this environment using pip and the
requirements.txt using `pip install -r requirement.txt`

#### Running CNN on GPU

To be able to run the CNN script on the GPU it is needed to install spacy with
GPU support. To make this happen please also run `pip install thinc_gpu_ops
thinc[cuda#your_cuda_version#] spacy[cuda]` to install the needed packages.
Since the test machine has cuda 11.1 the version number would be 111 (altough
    this is not supported by the thinc library yet)

If this is not done, the CNN Training is being run on the CPU which will take
more time.

#### Other requirements

To run the CNN Model with a base the model needs to be downloaded using the
spacy command. The model used for the thesis is named `de_core_news_md` which
should be downloaded by issuing `spacy download de_core_news_md`

To be able to run the tfidf scripts nltk needs the stopswords list which should
be downloaded by running `python -m nltk.downloader stopwords`

## Running

To execute the code please prepare a folder with one or more csv files that
contain the data given the scheme in the thesis. Then just execute the relevant
.py file from each folder and give it the folder where the csv(s) are contained.

```
python bert.py ../data/agb/csv
python cnn.py ../data/agb/csv
python tfidf.py ../data/agb/csv
python bagging.py ../data/agb/csv
```

After training once, following folders should be created:

- reports: Containing the last evaluation of the run in form of a table
- figs: Confusion Matrix of the last evaluation
- models: Model files of the training

## Project Structure

Before running any trainings it is recommended to read how data is structured
from the thesis. Further it is recommended to setup a dataset inside the
``data`` folder in such a way:

```
data/
  transform.py
  agb/
    csv/
      document1.csv
      document2.csv
      document3.csv
      ...
    document1.json
    document2.json
    document3.json
    ...
  nda/
    csv/
      document1.csv
      ...
    document1.json
    ...
```

It is not needed to provide the data in json format, just the csv format is used
in training. The json format is just how the original data for the thesis has
been supplied, thus needing a transformation script to handle it easier. Only
the csv files are relevant. It is still recommended to setup in such a way so
that all scripts can work properly.

Each folder (bagging, bert etc.) contains the script necessary to train and
evaluate the models. Please refer to the scripts documentation for further
information.
