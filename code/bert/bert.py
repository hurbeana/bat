#!/usr/bin/env python
# coding: utf-8
"""
This script can be used to train and evaluate a BERT Classifier on a given
dataset in csv file format.

For more information please refer to the thesis, README, help and documentation
fo this script.

To train and evaluate a model execute:
    python bert.py PATH_TO_CSV_FOLDER

The script requires that

* plac
* sh
* torch
* transformers
* numpy
* pandas
* sklearn
* seaborn
* matplotlib
* tqdm

is installed.

The file can also be imported as a module and contains the following functions:
    * load_data - loads the csvs and prepares them for BERT, returns pytorch
    samplers, dataset and dataset name (folder up two where the csvs reside)
    * ret_dataloader - takes dataset and samples and returns dataloaders
    * ret_model - returns the transformers model
    * ret_optim - returns a AdamW optimizer
    * ret_scheduler - returns a scheduler for training
    * flat_accuarcy - calculates the flat accuracy for given labels and
    predicitons
    * format_time - formats seconds to hh:mm:ss
    * train - trains and evaluates a model with given dataset and samplers and
    saves the results
    * main - main function of the script
"""
from warnings import simplefilter

simplefilter(action="ignore", category=FutureWarning)

import plac
import sh

import torch
from torch.utils.data import (
    TensorDataset,
    random_split,
    DataLoader,
    SubsetRandomSampler,
    RandomSampler,
    SequentialSampler,
)
from transformers import (
    AutoModelForSequenceClassification,
    AutoTokenizer,
    AutoConfig,
    EvalPrediction,
    TrainingArguments,
    BertForSequenceClassification,
    AdamW,
    get_linear_schedule_with_warmup,
)
import numpy as np
import pandas as pd
import json
from pathlib import Path
from sklearn import feature_extraction, model_selection, metrics

import csv
import unicodedata
import re
import sys

import logging
import random
import time
import datetime

import seaborn as sns
import matplotlib.pyplot as plt

from tqdm.auto import tqdm

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("urllib3").setLevel(logging.ERROR)
logging.getLogger("sh").setLevel(logging.ERROR)
logging.getLogger("matplotlib").setLevel(logging.ERROR)
logger = logging.getLogger(__name__)

all_labels = []
label_to_id = {}
id_to_label = {}


def load_data(
    csvs_path: Path,
    hf_model_name: str = "bert-base-german-cased",
    cutoff: int = 128,
    tokenizer_outpath: Path = None,
):
    """Loads all of the csvs and prepares them for BERT by tokenization. Returns
    pytorch datasets and samplers which are needed for the train function.

    Parameters
    ----------
    csvs_path : pathlib.Path
        The folder where one or more cvs files reside with format "text;label"
    hf_model_name : str, optional
        The hugginface model name to be used in training and testing. Acts as a
        base model. Needed for tokenization. (default = "bert-base-german-cased")
    cutoff : int, optional
        The cutoff in number of tokens. Sentences longer (in tokens) than the
        given int will be discarded. (default = 128)
    tokenizer_outpath : pathlib.Path
        The path where to store the tokenizer of the model, which is needed when
        loading the model from disk. Always use the same folder where the model
        is being persisted.

    Returns
    -------
    pytorch.utils.data.TensorDataset
        The dataset for training with tokens, input ids and attention masks
    (pytorch.utils.data.SubsetRandomSampler,
    pytorch.utils.data.SubsetRandomSampler)
        Pytorch samplers for training and validation
    str
        The dataset name taken from the csv folders parent. For example:
        data/agb/csv results in agb being the dataset name.
    """
    global all_labels
    global label_to_id
    global id_to_label

    assert csvs_path.exists() and csvs_path.is_dir()

    ds_name = csvs_path.parent.name
    logger.info("[%s] starting loading", ds_name)

    dfs = [pd.read_csv(csv_path, sep=";") for csv_path in csvs_path.glob("*.csv")]

    df = pd.concat(dfs, ignore_index=True)
    df = df.rename(columns={"label": "y", "text": "text"})

    def normalize(x):
        try:
            return unicodedata.normalize("NFKD", x)
        except:
            return ""

    df["text"] = df["text"].apply(normalize)
    logger.debug("5 samples from loaded data")
    logger.debug("\n%s", df.sample(5))

    all_labels = list(sorted(df.y.unique()))
    label_to_id = {l: i for i, l in enumerate(all_labels)}
    id_to_label = {i: l for i, l in enumerate(all_labels)}

    logger.info("[%s] cleaning data", ds_name)
    df_clean = df[df.y.str.startswith("p_")].copy()
    df_clean.text = df_clean.text.str.strip().copy()
    df_clean.text = df_clean.text.replace("r'[^\w\s]+|\n", "").copy()
    df_clean = df_clean[~(df_clean.text == "")].copy()
    logger.debug("5 samples from cleaned data")
    logger.debug("\n%s", df.sample(5))

    logger.info("Number of loaded sentences: {}\n".format(df.shape[0]))

    sentences = df.text.values
    labels = df.y.values

    logger.info("[%s] Loading %s tokenizer", ds_name, hf_model_name)
    tokenizer = AutoTokenizer.from_pretrained(hf_model_name, do_lower_case=False)

    max_len = 0
    remove_ids = []
    tok_lens = []
    logger.info("[%s] finding max len for inputs", ds_name)
    for i, sent in enumerate(sentences):

        # Tokenize the text and add `[CLS]` and `[SEP]` tokens.
        input_ids = tokenizer.encode(sent, add_special_tokens=True)
        if len(input_ids) > cutoff:
            # print(
            # "removing entry\n{}\ndue to being too long ({} tokens) Its label was {}".format(
            # sent, len(input_ids), labels[i]
            # )
            # )
            remove_ids.append(i)
            continue
        # Update the maximum sentence length.
        tok_lens.append(len(input_ids))
        max_len = max(max_len, len(input_ids))

    sentences = np.delete(sentences, remove_ids)
    labels = np.delete(labels, remove_ids)
    logger.info("Max input sentence length (in tokens) is %s", max_len)

    # df_tok = pd.DataFrame(tok_lens, columns=["lengths"])
    # df_tok = df_tok.sort_values(by="lengths")
    # df_tok.plot.hist(bins=128, figsize=(20, 10))

    input_ids = []
    attention_masks = []

    logger.info("[%s] tokenizing input", ds_name)
    for sent in sentences:
        # `encode_plus` will:
        #   (1) Tokenize the sentence.
        #   (2) Prepend the `[CLS]` token to the start.
        #   (3) Append the `[SEP]` token to the end.
        #   (4) Map tokens to their IDs.
        #   (5) Pad or truncate the sentence to `max_length`
        #   (6) Create attention masks for [PAD] tokens.
        encoded_dict = tokenizer.encode_plus(
            sent,  # Sentence to encode.
            add_special_tokens=True,  # Add '[CLS]' and '[SEP]'
            max_length=128,  # Pad & truncate all sentences.
            truncation=True,
            # pad_to_max_length = True,
            padding="max_length",
            return_attention_mask=True,  # Construct attn. masks.
            return_tensors="pt",  # Return pytorch tensors.
        )

        # Add the encoded sentence to the list.
        input_ids.append(encoded_dict["input_ids"])

        # And its attention mask (simply differentiates padding from non-padding).
        attention_masks.append(encoded_dict["attention_mask"])

    # Convert the lists into tensors.
    if not isinstance(input_ids, torch.Tensor):
        input_ids = torch.cat(input_ids, dim=0)
    if not isinstance(attention_masks, torch.Tensor):
        attention_masks = torch.cat(attention_masks, dim=0)
    if any([isinstance(s, str) for s in labels]):
        labels = [label_to_id[l] for l in labels]
    if not isinstance(labels, torch.Tensor):
        labels = torch.tensor(labels)

    # Print sentence 0, now as a list of IDs.
    logger.debug("Original:\n%s", sentences[0])
    logger.debug("Token IDs:\n%s", input_ids[0])

    # Combine the training inputs into a TensorDataset.
    dataset = TensorDataset(input_ids, attention_masks, labels)

    # Create a 80-20 train-validation split.

    # Calculate the number of samples to include in each set.
    train_size = int(0.8 * len(dataset))
    val_size = len(dataset) - train_size

    targets = labels.numpy()
    # Divide the dataset by randomly selecting samples.
    # train_dataset, val_dataset = random_split(dataset, [train_size, val_size])
    train_idx, valid_idx = model_selection.train_test_split(
        np.arange(len(targets)), test_size=0.2, shuffle=True, stratify=targets
    )

    train_sampler = SubsetRandomSampler(train_idx)
    valid_sampler = SubsetRandomSampler(valid_idx)

    if tokenizer_outpath:
        outpath = tokenizer_outpath / ds_name
        outpath.mkdir(parents=True, exist_ok=True)
        logger.debug("saving tokenizer to %s", outpath)
        tokenizer.save_pretrained(str(outpath))

    logger.info("{:>5,} training samples".format(train_size))
    logger.info("{:>5,} validation samples".format(val_size))
    return dataset, (train_sampler, valid_sampler), ds_name
    # return train_dataset, val_dataset, ds_name


def ret_dataloader(train_dataset, samplers, batch_size: int = 8):
    """Returns a pytorch dataloader given the dataset and samplers

    Parameters
    ----------
    train_dataset : pytorch.utils.data.TensorDataset
        The pytorch TensorDataset object with all tokens, input ids and
        attention masks from load_data
    samplers : (pytorch.utils.data.SubsetRandomSampler,
    pytorch.utils.data.SubsetRandomSampler)
        Pytorch samplers for training and validation from load_data
    batch_size : int, optional
        The batchsize to be used in training (default = 8)

    Returns
    -------
    pytorch.utils.data.DataLoader
        The pytorch dataloader that is being used in training
    pytorch.utils.data.DataLoader
        The pytorch dataloader that is being used in validation
    """
    logger.info("Creating dataloaders")
    logger.info("batch_size %s", batch_size)
    train_dataloader = DataLoader(
        train_dataset,  # The training samples.
        # sampler=RandomSampler(train_dataset),  # Select batches randomly
        sampler=samplers[0],
        batch_size=batch_size,  # Trains with this batch size.
    )

    validation_dataloader = DataLoader(
        # val_dataset,  # The validation samples.
        train_dataset,
        # sampler=SequentialSampler(val_dataset),  # Pull out batches sequentially.
        sampler=samplers[1],
        batch_size=batch_size,  # Evaluate with this batch size.
    )
    return train_dataloader, validation_dataloader


def ret_model(hf_model_name: str = "bert-base-german-cased"):
    """(Down)Loads the huggingface base model to be used in training and returns
    it.

    Parameters
    ----------
    hf_model_name : str, optional
        Hugginface model name which can be found in the hugginface model
        library (default = "bert-base-german-cased")

    Returns
    -------
    pytorch.Model
        A pytorch model containing the loaded model
    """
    global label_to_id
    global id_to_label
    logger.info("loading model %s", hf_model_name)
    model = AutoModelForSequenceClassification.from_pretrained(
        hf_model_name,
        num_labels=len(all_labels),
        id2label=id_to_label,
        label2id=label_to_id,
        output_attentions=False,  # Whether the model returns attentions weights.
        output_hidden_states=False,  # Whether the model returns all hidden-states.
    )

    return model


def ret_optim(model, learning_rate: float = 5e-05):
    """Returns a AdamW optimizer for training given the model and learning rate

    Parameters
    ----------
    model : pytorch.Model
        A loaded hugginface model
    learning_rate : float, optional
        The learning rate for training

    Returns
    -------
    torch.optim.AdamW
        The AdamW optimizer created for this model, to be used in training
    """
    logger.info("creating optimizer")
    logger.info("learning_rate %s", learning_rate)
    optimizer = AdamW(model.parameters(), lr=learning_rate, eps=1e-8)
    return optimizer


def ret_scheduler(train_dataloader, optimizer, epochs: int = 10):
    """Returns a scheduler to be used for training given dataloader and
    optimizer

    Parameters
    ----------
    train_dataloader : pytorch.utils.data.DataLoader
        The pytorch dataloader that is being used in training
    optimizer : torch.optim.AdamW
        The optimizer that is being used in training
    epochs : int, optional
        amount of epochs to train the model

    Returns
    -------
    torch.optim.lr_scheduler.LambdaLR
        The scheduler used for training
    """

    logger.info("creating scheduler")
    logger.info("training epochs %d", epochs)
    # Total number of training steps is [number of batches] x [number of epochs].
    # (Note that this is not the same as the number of training samples).
    total_steps = len(train_dataloader) * epochs

    logger.info("scheduling for total %d", total_steps)

    # Create the learning rate scheduler.
    scheduler = get_linear_schedule_with_warmup(
        optimizer,
        num_warmup_steps=0,  # Default value in run_glue.py
        num_training_steps=total_steps,
    )
    return scheduler


# Function to calculate the accuracy of our predictions vs labels
def flat_accuracy(preds, labels):
    """Calculates flat accuracy given a list of prediction and a list of true
    labels

    Parameters
    ----------
    preds : list[str]
        All predictions the model made
    labels : list[str]
        All true labels the model should make

    Returns
    -------
    float
        The accuarcy the model achieved
    """
    pred_flat = np.argmax(preds, axis=1).flatten()
    labels_flat = labels.flatten()
    return np.sum(pred_flat == labels_flat) / len(labels_flat)


def format_time(elapsed):
    """Takes a time in seconds and returns a string hh:mm:ss

    Parameters
    ----------
    elapsed : float
        The time that elapsed in seconds

    Returns
    -------
    str
        A formatted string in form of hh:mm:ss
    """
    # Round to the nearest second.
    elapsed_rounded = int(round((elapsed)))

    # Format as hh:mm:ss
    return str(datetime.timedelta(seconds=elapsed_rounded))


# https://github.com/huggingface/transformers/blob/5bfcd0485ece086ebcbed2d008813037968a9e58/examples/run_glue.py#L128
def train(
    train_ds,
    samplers,
    output_path: Path,
    hf_model_name: str,
    cutoff: int,
    batch_size: int,
    learning_rate: float,
    epochs: int,
    ds_name: str = "dsname",
    model_output_path: Path = Path("models"),
):
    """Trains and validates a given hugginface model and optionally saves it for
    later use. This function also creates folders ``reports``, ``figs`` and
    ``models`` by default to store all results.

    In ``reports`` the function saves the last classfication report of validation.
    In ``figs`` the function saves the last confusion matrix of validation.
    In ``models`` the function persists the pytorch Model that can be later used
    with the transformers hugginface library.

    Parameters
    ----------
    train_ds : pytorch.utils.data.DataLoader
        A pytorch DataLoader containing all tokens, input ids and attentions
        masks used for training and validation. See ret_dataloader and load_data
        for more information.
    samplers : (pytorch.utils.data.RandomSubsetSampler,
    pytorch.utils.data.RandomSubsetSampler)
        Two samplers (one for training and one for validation) to sample the
        dataset. See load_data for more information.
    output_path : pathlib.Path
        Path to where to save the evaluation to. This creates a folder "reports"
        and one "figs".
    hf_model_name : str
        The hugginface model name to be used when loading the model. See
        ret_model for more information.
    cutoff : int
        The cutoff for tokens when loading the data. See load_data for more
        information.
    batchsize : int
        Batch size to be used in training. See ret_dataloader for more
        information.
    learning_rate : float
        Learning rate to be used in training. See ret_optim for more
        information.
    epochs : int
        Epochs to train for. See ret_scheduler for more information.
    ds_name : str, optional
        Datset name used for saving and logging (default is "dsname")
    model_output_path : pathlib.Path
        Ouput Path to where to persist the model to (default is ./models)

    See Also
    --------
    load_data : Loading the necessary data to get train_ds and the samplers. The
    cutoff is also being passed to load_data.
    ret_dataloader, ret_model, ret_optim, ret_scheduler
    """
    

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    logger.info("device used %s", device)
    model = ret_model(hf_model_name)
    model.to(device)
    train_dataloader, validation_dataloader = ret_dataloader(
        train_ds, samplers, batch_size
    )
    optimizer = ret_optim(model, learning_rate)
    scheduler = ret_scheduler(train_dataloader, optimizer, epochs)
    seed_val = 42
    random.seed(seed_val)
    np.random.seed(seed_val)
    torch.manual_seed(seed_val)
    torch.cuda.manual_seed_all(seed_val)
    training_stats = []
    total_t0 = time.time()
    t_e = tqdm(list(range(0, epochs)), leave=False)
    for epoch_i in t_e:
        t_e.set_description(f"At Epoch {epoch_i}")
        t0 = time.time()
        total_train_loss = 0
        # `train` just changes the *mode*, it doesn't *perform* the training.
        # `dropout` and `batchnorm` layers behave differently during training
        model.train()
        t_b = tqdm(list(enumerate(train_dataloader)))
        for step, batch in t_b:
            t_b.set_description("Running Batches")
            # Progress update every 40 batches.
            if step % 40 == 0 and not step == 0:
                # Calculate elapsed time in minutes.
                elapsed = format_time(time.time() - t0)

                # Report progress.
                #print(
                    #"  Batch {:>5,}  of  {:>5,}.    Elapsed: {:}.".format(
                        #step, len(train_dataloader), elapsed
                    #)
                #)
            # `batch` contains three pytorch tensors:
            #   [0]: input ids
            #   [1]: attention masks
            #   [2]: labels
            b_input_ids = batch[0].to(device)
            b_input_mask = batch[1].to(device)
            b_labels = batch[2].to(device)
            # Always clear any previously calculated gradients before performing a
            # backward pass.
            model.zero_grad()
            # Perform a forward pass (evaluate the model on this training batch).
            # The documentation for this `model` function is here:
            # https://huggingface.co/transformers/v2.2.0/model_doc/bert.html#transformers.BertForSequenceClassification
            out = model(
                b_input_ids,
                token_type_ids=None,
                attention_mask=b_input_mask,
                labels=b_labels,
            )
            loss = out.loss
            logits = out.logits
            #logger.info({"train_batch_loss": loss.item()})
            # Accumulate the training loss over all of the batches so that we can
            # calculate the average loss at the end. `loss` is a Tensor containing a
            # single value; the `.item()` function just returns the Python value
            # from the tensor.
            total_train_loss += loss.item()
            # Perform a backward pass to calculate the gradients.
            loss.backward()
            # Clip the norm of the gradients to 1.0.
            # This is to help prevent the "exploding gradients" problem.
            torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
            # Update parameters and take a step using the computed gradient.
            # The optimizer dictates the "update rule"--how the parameters are
            # modified based on their gradients, the learning rate, etc.
            optimizer.step()
            # Update the learning rate.
            scheduler.step()
        avg_train_loss = total_train_loss / len(train_dataloader)
        training_time = format_time(time.time() - t0)
        logger.info({"avg_train_loss": avg_train_loss})
        logger.info("")
        logger.info("  Average training loss: {0:.2f}".format(avg_train_loss))
        logger.info("  Training epoch took: {:}".format(training_time))
        logger.info("")
        logger.info("Running Validation")

        t0 = time.time()
        model.eval()
        total_eval_accuracy = 0
        total_input_ids = []
        total_pred_ids = []
        total_eval_loss = 0
        nb_eval_steps = 0
        for batch in validation_dataloader:
            # `batch` contains three pytorch tensors:
            #   [0]: input ids
            #   [1]: attention masks
            #   [2]: labels
            b_input_ids = batch[0].cuda()
            b_input_mask = batch[1].to(device)
            b_labels = batch[2].to(device)
            # Tell pytorch not to bother with constructing the compute graph during
            # the forward pass, since this is only needed for backprop (training).
            with torch.no_grad():
                # Forward pass, calculate logit predictions.
                # token_type_ids is the same as the "segment ids", which
                # differentiates sentence 1 and 2 in 2-sentence tasks.
                # The documentation for this `model` function is here:
                # https://huggingface.co/transformers/v2.2.0/model_doc/bert.html#transformers.BertForSequenceClassification
                # Get the "logits" output by the model. The "logits" are the output
                # values prior to applying an activation function like the softmax.
                out = model(
                    b_input_ids,
                    token_type_ids=None,
                    attention_mask=b_input_mask,
                    labels=b_labels,
                )
                loss = out.loss
                logits = out.logits
            # Accumulate the validation loss.
            total_eval_loss += loss.item()
            # Move logits and labels to CPU
            logits = logits.detach().cpu().numpy()
            label_ids = b_labels.to("cpu").numpy()
            # Calculate the accuracy for this batch of test sentences, and
            # accumulate it over all batches.
            total_eval_accuracy += flat_accuracy(logits, label_ids)
            pred_flat = np.argmax(logits, axis=1).flatten()
            total_input_ids.extend(label_ids)
            total_pred_ids.extend(pred_flat)
        # Report the final accuracy for this validation run.
        avg_val_accuracy = total_eval_accuracy / len(validation_dataloader)
        logger.debug("  Accuracy: {0:.2f}".format(avg_val_accuracy))
        # Calculate the average loss over all of the batches.
        avg_val_loss = total_eval_loss / len(validation_dataloader)
        # Measure how long the validation run took.
        validation_time = format_time(time.time() - t0)
        logger.debug({"val_accuracy": avg_val_accuracy, "avg_val_loss": avg_val_loss})
        logger.debug("  Validation Loss: {0:.2f}".format(avg_val_loss))
        logger.debug("  Validation took: {:}".format(validation_time))
        report = metrics.classification_report(
            total_input_ids,
            total_pred_ids,
            output_dict=False,
            labels=range(0, len(all_labels)),
            target_names=all_labels,
        )
        # Record all statistics from this epoch.
        training_stats.append(
            {
                "epoch": epoch_i + 1,
                "Training Loss": avg_train_loss,
                "Valid. Loss": avg_val_loss,
                "Valid. Accur.": avg_val_accuracy,
                "Training Time": training_time,
                "Validation Time": validation_time,
            }
        )
        logger.info(report)
        if epoch_i == epochs - 1:
            logger.info("writing figs")
            figs_path = output_path / "figs"
            sh.mkdir("-p", str(figs_path))
            # Plot confusion matrix
            inp = [id_to_label[i] for i in total_input_ids]
            pred = [id_to_label[i] for i in total_pred_ids]
            cm = metrics.confusion_matrix(inp, pred, labels=all_labels)
            df_cm = pd.DataFrame(cm, index=all_labels, columns=all_labels,)
            fig, ax = plt.subplots(figsize=(20, 20))
            heatmap = sns.heatmap(
                df_cm, annot=True, fmt="d", ax=ax, cmap=plt.cm.Blues_r, cbar=False
            )
            plt.title("Confusion matrix of the classifier")
            heatmap.yaxis.set_ticklabels(
                heatmap.yaxis.get_ticklabels(), rotation=0, ha="right", fontsize=14
            )
            heatmap.xaxis.set_ticklabels(
                heatmap.xaxis.get_ticklabels(), rotation=45, ha="right", fontsize=14
            )
            ax.set(xlabel="Pred", ylabel="True")
            # ax.set(xlabel="Pred", ylabel="True", xticklabels=classes,
            #       yticklabels=classes, title="Confusion matrix")
            plt.yticks(rotation=0)
            plt.savefig(figs_path / f"{ds_name}_cm.png")

            logger.info("writing report")
            reports_path = output_path / "reports"
            sh.mkdir("-p", str(reports_path))
            report_path_file = reports_path / f"{ds_name}_rep.txt"
            report_path_file.write_text(report)
            with report_path_file.open("a") as f:
                f.write(f"Total training time: {format_time(time.time() - total_t0)}\n")
                for stat in training_stats:
                    f.write(str(stat))
                    f.write("\n")
    logger.info("")
    logger.info("Training complete!")
    logger.info(
        "Total training took {:} (h:mm:ss)".format(format_time(time.time() - total_t0))
    )
    ds_outpath = model_output_path / ds_name
    ds_outpath.mkdir(parents=True, exist_ok=True)
    model.save_pretrained(str(ds_outpath))


def main(
    data_path: ("Path to folder with csvs", "positional", None, Path),
    output_path: ("Path where to write stats to", "positional", None, Path) =
    Path.cwd(),
    hf_model_name: (
        "Huggingface model name to be used",
        "option",
        "m",
    ) = "bert-base-german-cased",
    cutoff: ("Cutoff for tokenization", "option", "t") = 128,
    batch_size: ("Batch size for training", "option", "b") = 8,
    learning_rate: ("Learning rate for training", "option", "l") = 5e-5,
    epochs: ("Epochs for learning", "option", "e") = 10,
    model_output_path: ("Path to where to save model", "option", "o") = Path("models"),
):
    """The main function to run load and prepare data and the run training and
    validation on a given huggingface model.

    Parameters
    ----------
    data_path : pathlib.Path
        The path where the csvs reside. Preferrably the parents folder is named
        like the dataset (such as agb/csv)
    output_path : pathlib.Path, optional
        The path where to save statistical data of validation to. (default =
        Path.cwd())
    hf_model_name : str, optional
        Huggingface model name to be used as base for training (default =
        "bert-base-german-cased")
    cutoff : int, optional
        Cutoff to be used when discard sentences that have too many tokens for
        bert (default = 128)
    batch_size : int, optional
        Batch size to be used during training (default = 8)
    learning_rate : float, optional
        Learning rate to be used during training (default = 5e-5)
    epochs : int, optional
        Epochs to train for (default = 10)
    model_output_path : pathlib.Path
        Output folder to where to output model and tokenizer when persisting the
        model (default = ./models)
    """
    train_ds, samplers, ds_name = load_data(
        data_path, hf_model_name, tokenizer_outpath=model_output_path
    )
    train(
        train_ds,
        samplers,
        output_path,
        hf_model_name,
        cutoff,
        batch_size,
        learning_rate,
        epochs,
        ds_name,
        model_output_path,
    )


if __name__ == "__main__":
    plac.call(main)
