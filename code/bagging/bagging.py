"""
This script can be used to train and evaluate a Bagging Classifier on a given
dataset in csv file format

For more information please refer to the thesis, README, help and documentation
of this script.

To train and evaluate a model execute:
    python bagging.py PATH_TO_CSV_FOLDER

To run this script

* pattern
* plac
* nltk
* sklearn
* pandas
* numpy
* tqdm
* sh
* matplotlib
* seaborn

need to be installed.
"""
import os
from pattern.vector import Document, Model, NB, KNN, SLP, Model
from pathlib import Path
import plac

import logging
import sklearn.metrics
import time
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import metrics
import re
import nltk
from tqdm import tqdm
import sh
import matplotlib.pyplot as plt
import seaborn as sns

RANDOM_SEED=42
np.random.seed(RANDOM_SEED)

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
logging.getLogger("matplotlib").setLevel(logging.ERROR)
logging.getLogger("sh").setLevel(logging.ERROR)


class Bagging_Classifier:
    """
    A class to represent the bagging classifier which can be reused for evaluation

    Attributes
    ----------
    doctype : str
        the doctype which this classifier has been trained on. For example "agb"
    nb : pattern.vector.NB
        the Naive Bayes classifier for the bagging classifier
    knn : pattern.vector.KNN
        the K Nearest Neighbors classifier for the bagging classifier
    slp : pattern.vector.SLP
        the Single Layer Perceptron classifier for the bagging classifier
    total_train_time : float
        the total training time after running the train function

    Methods
    -------
    preprocessing(text)
        preprocesses the given text so that it can be classified or used in training
    run_on_df(df)
        runs the classifier on each row of a pandas dataframe and sets the
        predictions in a column "pred"
    predict_label(p_text)
        classifies the text with the classifier and returns the label
    train(df_train)
        trains all three models with the given training pandas dataframe
    get_metrics(df, ds_name)
        runs the needed functions to get metrics for the dataframe which needs
        to contain a column with predictions and true labels. The dataset name
        is needed to save the results to disk
    eval(test_df)
        evaluates the bagging classifier on a given test pandas dataframe. The
        dataframe needs to contain true labels and text.
    save_to(save_path)
        persists the classifier to disk
    """

    def __init__(
        self,
        doctype: str,
        model_path: Path = None,
        data_path: Path = None,
        eval_model: bool = True,
    ):
        """
        Parameters
        ----------
        doctype : str
            The given doctype of this classifier (such as "agb")
        model_path : pathlib.Path, optional
            The path from where the models can be loaded (default is None for
            empty model). The model_path folder should contain files for each
            model such as:
                - agb.slp
                - agb.knn
                - agb.nb
        data_path : pathlib.Path, optional
            The path where the dataset is stored in csv format. Used for
            training (default is None)
        eval_model : bool, optional
            Whether to evaluate the model after training (default is True)
        """
        self.doctype = doctype

        def _load_model(model_type):
            """
            Load the model from disk given the model_type (knn, slp or nb),
            doctype and model_path.

            Parameters
            ----------
            model_type : str
                The model_type that is being loaded (knn, slp or nb)
            """
            logger.debug("loading model %s for %s", model_type, doctype)
            p = model_path / (doctype + "." + model_type)
            if os.path.isfile(p):
                setattr(self, model_type, Classifier.load(p))
            else:
                logger.debug(
                    "failed loading model_type %s from %s defaulting to sonstiges",
                    model_type,
                    p,
                )
                setattr(
                    self,
                    model_type,
                    Classifier.load(model_path / ("sonstiges." + model_type)),
                )

        def _load_data(clean=True, split=0.2):
            """
            Load all csvs that can be found in the data_path and return a split
            from training and testing

            Parameters
            ----------
            clean : str, optional
                Whether to clean the each text with preprocessing before
                returning (default is True)
            split : float, optional
                How many percent of the dataset to use for testing (default is
                0.2)

            Returns
            -------
            pandas.DataFrame
                train dataframe for training
            pandas.DataFrame
                test dataframe for testing
            """
            logger.info("Loading Data")
            dfs = [pd.read_csv(f, sep=";") for f in data_path.glob("*.csv")]
            df = pd.concat(dfs, ignore_index=True)
            df["text"] = df["text"].astype(str)
            df["label"] = df["label"].astype(str)
            if clean:
                df = df[df["text"] != ""]
                df["text"] = df["text"].apply(self.preprocessing)

            return train_test_split(df, test_size=split)

        if model_path:
            _load_model("nb")
            _load_model("knn")
            _load_model("slp")

        if data_path:
            train_df, test_df = _load_data()
            self.total_train_time = self.train(train_df)
            if eval_model:
                self.eval(test_df)

    def preprocessing(self, text):
        """
        Simple preprocessing for text so that the model performs better. Takes
        in the given text and returns it back cleaned.

        Parameters
        ----------
        text : str
            The text that needs to be preprocessed

        Returns
        -------
        str
            Text that has been preprocessed
        """
        lst_stopwords = nltk.corpus.stopwords.words("german")
        st_text = re.sub(r"(^(\d|\.)+)|^(\(?.{,5}\))", "", text)
        text = re.sub(r"[^\w\s]", "", str(st_text).lower().strip())

        lst_text = text.split()

        if lst_stopwords is not None:
            lst_text = [word for word in lst_text if word not in lst_stopwords]

        text = " ".join(lst_text)
        return text

    def run_on_df(self, df: pd.DataFrame):
        """
        Runs the function self.predict_label on all texts in the dfs column
        named "text" and saves them to a new column named "pred". Model needs
        to be loaded or trained before this function can be run.

        Parameters
        ----------
        df : pandas.DataFrame
            Dataframe containing a "text" column with texts

        Returns
        -------
        pandas.DataFrame
            Dataframe with new column "pred" containing predicitions from the
            model
        """
        logger.debug("running Bagging_Classifier with %s", self.doctype)

        def _exec_c(r):
            c = None
            try:
                c = self.predict_label(r)
            except TypeError:
                logger.error("Could not predict %s", repr(r))
            return c if c else "p_other"

        tqdm.pandas()
        df["pred"] = df["text"].progress_apply(_exec_c)
        # df["pred"] = df["text"].apply(lambda x: _exec_c(classifier, x))
        logger.debug("done running classifier")
        return df

    def predict_label(self, p_text):
        """
        Predicts a label using the loaded models and returns the result

        Parameters
        ----------
        p_text : str
            The text that needs classification

        Returns
        -------
        str
            Label that the model predicts for the given text
        """
        d = Document(self.preprocessing(p_text))
        nb_label = self.nb.classify(d)
        slp_label = self.slp.classify(d)
        knn_label = self.knn.classify(d)
        if knn_label == slp_label:
            return knn_label
        else:
            return nb_label

    def train(self, df_train):
        """
        Trains all three empty models using the "text" column in df_train and
        "label" column in df_train. Returns the time that this took.

        Parameters
        ----------
        df_train : panads.DataFrame
            The DataFrame containing columns "text" and "label"

        Returns
        -------
        int
            Time in second how long the training took
        """
        logger.info("Starting Training")
        docs = []
        for i, row in df_train.iterrows():
            d = Document(row.text, type=row.label)
            docs.append(d)

        t = time.time()
        m = Model(docs)
        t_t = tqdm(total=3)
        t_t.set_description("Training Model NB")
        self.nb = NB(train=m)
        t_t.update(1)
        t_t.set_description("Training Model SLP")
        self.slp = SLP(train=m)
        t_t.update(1)
        t_t.set_description("Training Model KNN")
        self.knn = KNN(train=m)
        t_t.update(1)
        t = time.time() - t
        logger.info("Training took %s s", t)
        return t

    def get_metrics(self, df: pd.DataFrame, ds_name: str):
        """
        Calculates metrics on the dataframe with columns "label" and "pred" and
        saves them to reports/ds_name_rep.txt and figs/ds_name_cm.png.

        Parameters
        ----------
        df : pd.DataFrame
            The Dataframe containing true labels (in column "label") and
            predicted labels (in column "pred")
        ds_name : str
            The name of the dataset (for saving purposes)
        """
        logger = logging.getLogger("metrics")
        logger.info("[%s] starting metrics", ds_name)
        classes = df.label.unique()

        logger.debug("getting accuracy")
        accuracy = metrics.accuracy_score(df.label, df.pred)
        logger.debug("accuarcy for %s: %f", ds_name, accuracy)
        sh.mkdir("-p", "reports")
        logger.debug("running classification_report for %s", ds_name)
        rep = metrics.classification_report(df.label, df.pred, zero_division=0)
        (Path("reports") / f"{ds_name}_rep.txt").write_text(rep)
        logger.debug(rep)

        ## Plot confusion matrix
        cm = metrics.confusion_matrix(df.label, df.pred, labels=classes)

        df_cm = pd.DataFrame(cm, index=classes, columns=classes,)
        fig, ax = plt.subplots(figsize=(20, 20))
        heatmap = sns.heatmap(
            df_cm, annot=True, fmt="d", ax=ax, cmap=plt.cm.Blues_r, cbar=False
        )
        plt.title("Confusion matrix of the classifier")
        heatmap.yaxis.set_ticklabels(
            heatmap.yaxis.get_ticklabels(), rotation=0, ha="right", fontsize=14
        )
        heatmap.xaxis.set_ticklabels(
            heatmap.xaxis.get_ticklabels(), rotation=45, ha="right", fontsize=14
        )
        ax.set(xlabel="Pred", ylabel="True")
        # ax.set(xlabel="Pred", ylabel="True", xticklabels=classes,
        #       yticklabels=classes, title="Confusion matrix")
        plt.yticks(rotation=0)
        sh.mkdir("-p", "figs")
        plt.savefig(f"figs/{ds_name}_cm.png")

    def eval(self, test_df):
        """
        Evaluates the classifier using self.run_on_df and self.get_metrics and
        the test_df DataFrame

        Parameters
        ----------
        test_df : pandas.DataFrame
            The dataframe containing "text" and "label" columns to test on
        """
        report_path = Path("reports") / f"{self.doctype}_rep.txt"
        fig_path = Path("figs") / f"{self.doctype}_cm.png"
        start = time.time()
        df_pred = self.run_on_df(test_df)
        end = time.time()
        logger.info("[%s] Done with classification in %f s", self.doctype, end - start)
        self.get_metrics(df_pred, self.doctype)
        with report_path.open("a") as f:
            f.write(f"TOTAL TRAIN TIME: {self.total_train_time}\n")
            f.write(f"TOTAL EVAL TIME: {end-start}\n")

    def save_to(self, save_path):
        """
        Persists the models to disk. In the save_path there will be 3 Files
        created (one for each model i.e. agb.slp, agb.nb and agb.knn).
        
        Parameters
        ----------
        save_path : str
            The path to where to save the models to, does not need to exist"
        """
        logger.debug("Saving models to %s", save_path)
        save_path = Path(save_path)
        self.nb.save((save_path / (self.doctype + ".nb")), final=True)
        self.knn.save((save_path / (self.doctype + ".knn")), final=True)
        self.slp.save((save_path / (self.doctype + ".slp")), final=True)


def main(
    data_path: "Path to data to scan for datasets",
    save_path: "Path to save models to" = "models",
):
    """Main function of this script that reads data from data_path, trains the
    model, evaluates it, writes the results to ``reports`` and ``figs`` and
    saves the model to save_path

    Paramters
    ---------
    data_path : pathlib.Path, optional
        Folder containing one or multiple csvs used for training and validation
    save_path : str
        Folder to save the model to (default = ./models)
    """
    classify = Bagging_Classifier(data_path.parent.name, data_path=data_path)
    if save_path:
        classify.save_to(save_path)


if __name__ == "__main__":
    plac.call(main)
