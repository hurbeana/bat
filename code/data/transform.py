"""
This script can be used to convert json formatted files containing whole
documents with all sorts of annotations to csv files that only contain
annotations for singular sentences that can be used for training. This script
tries to read everything as a json file but falls back to trying to parse a
[GATE](https://gate.ac.uk/) xml file if possible.

For more information on document and annotation structure please refer to the
thesis, README, help and documentation of this script.

To run data transformation execute:
    ```python transform.py PATH_TO_JSON_FOLDER```

For example if a folder named "agb" exists with several json files, each
being one singular document:
    agb/
        doc1.json
        doc2.json
        ...
Then running ``python transform.py agb`` will run this script transforming every
document and creating a ``csv`` folder inside the agb folder containing all of
the transformed documents.

To run this script 

* plac
* pandas
* sh
* bs4

need to be installed.

This script can be imported as a module which contains following functions:
    * strip_numeration - strips the leading numeration of a string
    * parse_xml - fallback for when opening the file as json doesn't work. This
    could mean the file is a GATE XML document. This function can parse such a
    file.
    * main - main function which processes all json or GATE documents in a
    folder and writes the to a "csv" folder in the same directory as the
    documents
"""
from pathlib import Path
import plac
from json import dumps, loads
from json.decoder import JSONDecodeError
import logging
from typing import List, Dict
import pandas as pd
import sh
from bs4 import BeautifulSoup as bs
import re

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("sh").setLevel(logging.ERROR)
logging.getLogger("parse_xml").setLevel(logging.ERROR)
logger = logging.getLogger(Path(__file__).stem)

# replacement strings
WINDOWS_LINE_ENDING = "\r\n"
CARRIAGE_RETURN = "\r"
UNIX_LINE_ENDING = "\n"


def strip_numeration(txt):
    """ Strips all of the enumeration characters at the beginning of a sentence.
    Examples are:
        1.1
        3.
        4
        c)
        (5)
        I.
        I)
        C)

    Parameters
    ----------
    txt : str
        The text that possibly has numeration which needs to be removed

    Returns
    -------
    str
        The same text with any leading numeration stripped
    """
    return re.sub(r"(^(\d+|\.)+ ?)|^(\(?.*\)) ?|(^[IVXCivxc]+ ?\.)", "", txt).strip()


def parse_xml(xml, xml_name, output_dir):
    """Parses GATE XML documents to csv files and places them into output_dir

    Parameters
    ----------
    xml : str
        The xml file contents that need to parsed
    xml_name : str
        The xml file name (used for logging and saving the csv)
    output_dir : pathlib.Path
        The output directory to where the resulting csv files are stored
    """
    logger = logging.getLogger("parse_xml")
    logger.info("Running parse on %s", xml_name)
    soup = bs(xml, "lxml")
    logger.info("Read XML" if soup else "Could not read XML")
    nodes = soup.find_all("node")
    n_dict = {int(n.attrs["id"]): n.next_sibling for n in nodes}
    df_n = pd.DataFrame.from_dict(n_dict, orient="index")
    annotations = soup.find_all("annotation")
    n_annot = {}
    n_text = {}
    for index, node in df_n.iterrows():
        logger.debug("txt: %s", node.values[0])
        txt = node.values[0]
        if not txt:
            continue
        txt = txt.replace(WINDOWS_LINE_ENDING, " ")  # strip all line endings
        txt = txt.replace(CARRIAGE_RETURN, " ")  # strip all remaining carriage returns
        txt = " ".join(txt.split())  # strip multiple spaces
        txt = txt.strip()
        txt = strip_numeration(txt)
        if (
            not txt or len(txt.split(" ")) < 5
        ):  # strip too short sentences (mostly just random enumerations)
            logger.debug("skipping: %s", repr(node.values[0]))
            continue
        n_text[index] = txt
        for annot in annotations:
            a_id, a_type, start, end = annot.attrs.values()  # xml attributes
            if a_type != "paragraph" and index >= int(start) and index < int(end):
                n_annot[index] = a_type
                break
        if not index in n_annot:
            n_annot[index] = "p_other"
        # logger.debug("assigned annot: %s", n_annot[index])
    df_annot = pd.DataFrame(
        zip(n_annot.values(), n_text.values()), columns=["label", "text"]
    )
    logger.info("Writing output to %s", output_dir)
    output_dir = Path(output_dir)
    output_dir.mkdir(parents=True, exist_ok=True)
    df_annot.to_csv(output_dir / (Path(xml_name).stem + ".csv"), index=False, sep=";")


def main(
    data_dir: ("Path to directory with json training files", "positional", None, Path),
):
    """Main function which reads all json or GATE XML documents from the given
    data_dir, creates a new folder ``csv`` and writes all of the transformed
    csv files (which have the same name as the original documents) to the
    ``csv`` folder.

    Paramters
    ---------
    data_dir : pathlib.path
        The directory containing multiple json or GATE XML documents with
        annotations and text
    """
    to_parse = {}
    for f in data_dir.glob("*"):
        try:
            if f.is_file() and "cache" not in f.name:
                logger.debug("trying to load file %s", f)
                to_parse[f.name] = loads(f.read_text(encoding="utf-8"))
        except (FileNotFoundError, UnicodeError, IsADirectoryError,) as e:
            logger.error("failed to load file %s. MSG [%s]", f, e)
        except JSONDecodeError:
            logger.debug("failed to load with json, possibly xml")
            logger.debug("trying to load as gate doc")
            parse_xml(
                f.read_text(encoding="utf-8"), f.name, f.parent / "csv",
            )
            logger.debug("successfully parsed xml")

    for f, v in to_parse.items():
        try:
            logger.debug("parsing file %s", f)
            text = v["text"]
            found_annotations = []
            for annot in v["annotations"]:
                try:
                    t = annot.get("type", None)
                    if not t or not t.startswith("p_"):
                        t = annot.get("tag", None)
                    start, end = annot["start"], annot["end"]
                    if t and t.startswith("p_"):
                        found_annotations.append({"text": text[start:end], "label": t})
                except KeyError as e:
                    logging.error("keyerror[%s] on %s", f, e)
        except KeyError as e:
            logging.error("keyerror[%s] on %s", e, f)
        logger.debug("found total %d annotations", len(found_annotations))
        df = pd.DataFrame(found_annotations)
        out_dir = data_dir / "csv"
        out_file = Path(f).stem + ".csv"
        sh.mkdir("-p", str(out_dir))
        logger.debug("saving to %s", out_dir / out_file)
        df.to_csv(out_dir / out_file, sep=";", index=False)


if __name__ == "__main__":
    plac.call(main)
