#!/usr/bin/env python
# coding: utf-8
"""
This script can be used to train and evaluate a TFIDF + Multinomial Bayes
Classifier on a given dataset in csv file format.

For more information please refer to the thesis, README, help and documentation
fo this script.

To train and evaluate a model execute:
    python tfidf.py PATH_TO_CSV_FOLDER

The script requires that

* pandas
* plac
* nltk
* matplotlib
* seaborn
* sklearn

are installed.

The file can also be imported as a module and contains the following functions:
    * load_data - loads the csvs and prepares them for training, returns a
    pandas dataframe for training and one for testing including all of the labels
    that were found.
    * train - trains a sklearn model with given dataset 
    * test - evaluates a sklearn model and saves the results to ``reports`` and
    ``figs``
    * main - main function of the script that loads data and runs training +
    testing
"""

import os
import sh

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

## for data
import json
from pathlib import Path
import pandas as pd
import numpy as np
import nltk
import re
import plac

## for plotting
import matplotlib.pyplot as plt
import seaborn as sns

## for bag-of-words
from sklearn import (
    feature_extraction,
    model_selection,
    naive_bayes,
    pipeline,
    manifold,
    preprocessing,
    feature_selection,
    metrics,
)
import logging
import time

RANDOM_STATE = 42
np.random.seed(RANDOM_STATE)

logging.basicConfig(level=logging.ERROR)
# logging.getLogger("matplotlib").setLevel(logging.ERROR)
# logging.getLogger("sh").setLevel(logging.ERROR)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def load_data(csv_path):
    """Loads and prepares all data from a given folder containing one or
    multiple csv files with columns "text";"label". It is preferred to name the
    parent folder like the dataset to ensure corect logging (such as agb/csv)

    Parameters
    ----------
    csv_path : pathlib.Path
        The folder path which contains one or more csvs with the needed columns
        for training and validation.

    Returns
    -------
    pandas.DataFrame
        DataFrame containing all preprocessed training examples with labels
    pandas.DataFrame
        DataFrame containing all preprocessed validatation examples with labels
    list[str]
        All labels that were found during loading in a list
    """
    assert csv_path.exists()

    dfs = [pd.read_csv(c, sep=";") for c in csv_path.glob("*.csv")]

    df = pd.concat(dfs, ignore_index=True)
    df = df.rename(columns={"label": "y", "text": "text"})
    labels = df.y.unique()
    df.sample(5)
    logger.debug("Loaded %d samples total", len(df.index))

    def plot_dist(df):
        """Takes a dataframe with a "y" column and plots a bargraph how many
        examples per label exist. Writes the result to "data_distribution.png"
        file in the cwd.

        Parameters
        ----------
        df : pandas.DataFrame
            The pandas DataFrame to be plotted.
        """
        fig, ax = plt.subplots(figsize=(15, 10))
        fig.suptitle("Anzahl der Samples pro Labels", fontsize=12)
        per_label = df["y"].reset_index().groupby("y").count().sort_values(by="index")
        logger.debug("\n%s", per_label.to_string())
        logger.debug("\n%s", per_label.to_latex())
        per_label.plot(kind="barh", legend=False, ax=ax).grid(axis="x")
        ax.set_xlabel("Anzahl")
        ax.set_ylabel("Label")
        # plt.show()
        plt.savefig("data_distribution.png", dpi=96)

    def downsample(df, sample_class, amount):
        """Downsamples the given class down to the given amount by randomly
        removing samples from the given sample_class.

        Parameters
        ----------
        df : pandas.DataFrame
            The pandas DataFrame containing "text" and "y" column that needs to
            be downsampled
        sample_class : str
            The class from "y" column that has too many examples and needs to be
            downsampled
        amount : int
            Amount of examples to keep from sample_class

        Returns
        -------
        pandas.DataFrame
            A pandas DataFrame with the same columns as the input DataFrame,
            with "amount" amount of samples in the sample_class
        """
        test_df = df.copy(deep=True)
        shuffled_df = test_df.sample(frac=1, random_state=4)
        non_p_df = shuffled_df.loc[shuffled_df["y"] != sample_class]
        p_df = shuffled_df.loc[shuffled_df["y"] == sample_class].sample(
            n=amount, random_state=42
        )
        return pd.concat([non_p_df, p_df])

    df = df[df["text"].str.len() > 35]
    logger.debug("Loaded %d samples left after removing short ones", len(df.index))
    # plot_dist(df)
    not_p_len = len(df[df["y"] != "p_other"].index)
    p_len = len(df[df["y"] == "p_other"].index)
    total = len(df.index)
    classes = len(df["y"].unique())
    average_per_class = round(not_p_len / classes)

    normalized_df = df.copy(deep=True)

    if False:
        ccounts = df.groupby("y").count()
        for too_large_class in ccounts[ccounts["text"] > average_per_class].iterrows():
            if too_large_class[0] == "p_other":
                normalized_df = downsample(
                    normalized_df, too_large_class[0], round(average_per_class * 1.2)
                )
                continue
            logger.debug(too_large_class[0])
            normalized_df = downsample(
                normalized_df, too_large_class[0], average_per_class
            )

    normalized_df = downsample(normalized_df, "paragraph", round(average_per_class*1.5))

    # plot_dist(normalized_df)

    def utils_preprocess_text(text, lst_stopwords=None):
        """cleans (convert to lowercase and remove punctuations and characters
        and then strip) the given text and returns it. Also uses stopwords list
        if given.

        Parameters
        ----------
        text : str
             The text that needs to be preprocessed
        lst_stopwords : list[str]
            The list of stopwords to be removed from the text

        Returns
        -------
        str
            The cleaned text
        """
        st_text = re.sub(r"(^(\d|\.)+)|^(\(?.{,5}\))", "", text)
        text = re.sub(r"[^\w\s]", "", str(st_text).lower().strip())

        # strip off leading numeration

        ## Tokenize (convert from string to list)
        lst_text = text.split()

        ## remove Stopwords
        if lst_stopwords is not None:
            lst_text = [word for word in lst_text if word not in lst_stopwords]

        # cs = nltk.stem.cistem.Cistem()
        # lst_text = [cs.stem(word) for word in lst_text]

        ## back to string from list
        text = " ".join(lst_text)
        return text

    lst_stopwords = nltk.corpus.stopwords.words("german")
    logger.debug("german stop words %s", lst_stopwords)

    df_clean = normalized_df.copy(deep=True)
    df_clean["text_clean"] = df_clean["text"].apply(
        lambda x: utils_preprocess_text(x, lst_stopwords=lst_stopwords)
    )

    logger.debug(df_clean.sample(5))

    ## return df_test, df_train
    df_train, df_test = model_selection.train_test_split(
        df_clean, test_size=0.2, stratify=df_clean.y
    )
    logger.debug(
        "Training with %d samples and testing with %d samples",
        len(df_train.index),
        len(df_test.index),
    )
    return df_train, df_test, labels


def train(df_train):
    """Trains a sklearn model with the given training DataFrame and return model
    and training time.

    Parameters
    ----------
    df_train : pandas.DataFrame
        The pandas DataFrame that contains a "y" and a "text" column that is
        being used to train the model
    
    Returns
    -------
    sklearn.pipeline.Pipeline
        The sklearn pipeline with vectorizer and classifier
    int
        The total training time in seconds
    """
    logger.info("started training")
    start = time.time()
    ## Tf-Idf (advanced variant of BoW)
    vectorizer = feature_extraction.text.TfidfVectorizer(
        max_features=100000, ngram_range=(1, 3)
    )

    corpus = df_train["text_clean"]
    X_train = vectorizer.fit_transform(corpus)
    dic_vocabulary = vectorizer.vocabulary_

    # sns.heatmap(
    # X_train.todense()[:, np.random.randint(0, X_train.shape[1], 100)] == 0,
    # vmin=0,
    # vmax=1,
    # cbar=False,
    # ).set_title("Sparse Matrix Sample")

    y = df_train["y"]
    X_names = vectorizer.get_feature_names()
    p_value_limit = 0.95
    df_features = pd.DataFrame()
    for cat in np.unique(y):
        chi2, p = feature_selection.chi2(X_train, y == cat)
        df_features = df_features.append(
            pd.DataFrame({"feature": X_names, "score": 1 - p, "y": cat})
        )
        df_features = df_features.sort_values(["y", "score"], ascending=[True, False])
        df_features = df_features[df_features["score"] > p_value_limit]
    X_names = df_features["feature"].unique().tolist()

    # for cat in np.unique(y):
    #   print("# {}:".format(cat))
    #   print("  . selected features:",
    #         len(df_features[df_features["y"]==cat]))
    #   print("  . top features:", ",".join(
    # df_features[df_features["y"]==cat]["feature"].values[:10]))
    #   print(" ")

    vectorizer = feature_extraction.text.TfidfVectorizer(vocabulary=X_names)
    vectorizer.fit(corpus)
    X_train = vectorizer.transform(corpus)
    dic_vocabulary = vectorizer.vocabulary_
    len(dic_vocabulary)

    classifier = naive_bayes.MultinomialNB()

    ## pipeline
    model = pipeline.Pipeline([("vectorizer", vectorizer), ("classifier", classifier)])
    ## train classifier
    model["classifier"].fit(X_train, df_train["y"].values)
    total_train_time = time.time() - start
    logger.info("finished training in %f s", total_train_time)
    return model, total_train_time


def test(
    model, df_test, model_name: str = "bababooey", total_train_time: int = 0, labels=[]
):
    """Tests a given sklearn model on the df_test DataFrame and saves the
    results to ``reports`` and ``figs``.

    Parameters
    ----------
    model : sklearn.pipeline.Pipeline
        A sklearn model or pipeline which can run predict on a list of string.
        This is the model that is being evaluated
    df_test : pandas.DataFrame
        The pandas DataFrame that contains all texts and labels that are being
        used for validation
    model_name : str, optional
        The name which is being used to save the files that are being created by
        this function. This name is being prepended to the filenames. (default
        = "bababooey")
    total_train_time : int
        The total training time that is also being written to the classification
        report in the ´´reports´´ folder
    labels : list[str]
        All of the labels that the model understands. This is needed so that the
        classification report can still work even if no examples are found for
        the a given label.
    """
    y_test = df_test["y"].values
    X_test = df_test["text_clean"].values
    logger.info("starting evaluation")
    start_eval = time.time()
    predicted = model.predict(X_test)
    total_prediction_time = time.time() - start_eval
    logger.info("finished evaluation in %f s", total_prediction_time)
    logger.info("starting prediction evaluation")
    start_eval = time.time()
    predicted_prob = model.predict_proba(X_test)
    total_prediction_proba_time = time.time() - start_eval
    logger.info("finished evaluation in %f s", total_prediction_proba_time)

    classes = np.unique(y_test)
    y_test_array = pd.get_dummies(y_test, drop_first=False).values

    ## Accuracy, Precision, Recall
    accuracy = metrics.accuracy_score(y_test, predicted)
    logger.info("Accuracy %f", accuracy)
    # auc = metrics.roc_auc_score(
    #    y_test, predicted_prob, multi_class="ovr", labels=labels
    # )
    # logger.info("AUC %f", auc)
    logger.info("Detail:")
    report = metrics.classification_report(y_test, predicted, zero_division=0)
    logger.info(report)
    # report += f"\nAUC {auc}"
    report += f"\nTotal TRAIN Time: {total_train_time:f} s"
    report += f"\nTotal EVAL Time: {total_prediction_time:f} s"
    report += f"\nTotal EVAL PROBA Time: {total_prediction_proba_time:f} s"
    report_file = Path("reports") / f"{model_name}_rep.txt"
    sh.mkdir("-p", "reports")
    report_file.write_text(report)

    ## Plot confusion matrix
    cm = metrics.confusion_matrix(y_test, predicted)
    df_cm = pd.DataFrame(cm, index=classes, columns=classes,)
    fig, ax = plt.subplots(figsize=(20, 20))
    heatmap = sns.heatmap(
        df_cm, annot=True, fmt="d", ax=ax, cmap=plt.cm.Blues_r, cbar=False
    )
    plt.title("Confusion matrix of the classifier")
    heatmap.yaxis.set_ticklabels(
        heatmap.yaxis.get_ticklabels(), rotation=0, ha="right", fontsize=14
    )
    heatmap.xaxis.set_ticklabels(
        heatmap.xaxis.get_ticklabels(), rotation=45, ha="right", fontsize=14
    )
    ax.set(xlabel="Pred", ylabel="True")
    # ax.set(xlabel="Pred", ylabel="True", xticklabels=classes,
    #       yticklabels=classes, title="Confusion matrix")
    plt.yticks(rotation=0)
    sh.mkdir("-p", "figs")
    plt.savefig(Path("figs") / f"{model_name}_cm.png")


def main(csv_path: ("Path to dir with csvs", "positional", None, Path)):
    """The main function of this script that loads the data from a csv folder
    and runs training + testing. This function also creates a ``reports`` and
    ``figs`` folder in the current working directory for persisting the testing
    reports.

    Parameters
    ----------
    csv_path : pathlib.Path
        The folder where one or multiple csvs reside and are being used for
        training and testing
    """
    train_df, test_df, labels = load_data(csv_path)
    model, total_train_time = train(train_df)
    test(
        model,
        test_df,
        csv_path.parent.name,
        total_train_time=total_train_time,
        labels=labels,
    )


if __name__ == "__main__":
    plac.call(main)
