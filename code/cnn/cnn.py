#!/usr/bin/env python
# coding: utf8
"""Train a convolutional neural network text classifier on the given dataset,
using the TextCategorizer component. The dataset will be loaded from a csv
folder containing one or multiple csvs with the needed format "text";"label".
The model is added to spacy.pipeline, and predictions are available via
`doc.cats`. This script has been taken from spacys samples and adjusted to fit
the present usecase.
For more details, see the documentation:
    * Training: https://spacy.io/usage/training

This script requires that

* plac
* tqdm
* spacy
* pandas
* numpy
* sh
* matplotlib
* seaborn

is installed.

The file can also be imported as a module and contains following functions:
    * main - the main function that loads data and runs training + validation
    * load_data - the function that loads all csvs files and prepares them for
    training and validation
    * sk_eval - sklearn evaluation of the training which can be run on the spacy
    pipeline components
"""
from __future__ import unicode_literals, print_function
import plac
import random
from pathlib import Path
import re
from sklearn import metrics
import time

import spacy
from spacy.util import minibatch, compounding
import pandas as pd

import logging
import sys
import numpy as np
import sh
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm.auto import tqdm
from tqdm import trange

RANDOM_SEED = 42
np.random.seed(RANDOM_SEED)
spacy.util.fix_random_seed(RANDOM_SEED)

logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


@plac.annotations(
    model=("Model name. Defaults to blank 'de' model.", "option", "m", str),
    ds_path=("Dataset csv path", "positional", None, Path),
    output_dir=("Optional output directory", "option", "o", Path),
    n_iter=("Number of training iterations", "option", "n", int),
)
def main(ds_path, model=None, output_dir=None, n_iter=20, sent=False):
    """The main function which loads data, a base or blank spacy model and runs
    training + validation. If a output directory is given, then the model will
    be persisted to disk.

    This function also creates the folder ``reports`` and ``figs`` where it
    stores sklearn classification reports in a textfile and a confusion matrix
    of the validation in form of a png.

    Parameters
    ----------
    model : str, optional
        Base model to be used in training. For better results it is highly
        recommended to download a spacy model such as "de_core_news_md" using
        the command ``spacy download de_core_news_md`` and provide this name to
        train with this model. (defaults to a blank de model)
    ds_path : pathlib.Path
        Path where the folder with csvs is stored. It is recommended to place
        the folder with a parent that contains the dataset name, such as
        ``agb/csv`` to ensure correct logging.
    output_dir : pathlib.Path, optional
        Output directory where to persist the model to. Can be loaded again by
        creating a spacy model with the given path ``nlp = spacy.load(PATH)``.
        (default = None)
    n_iter : int, optional
        Epochs to learn for (default = 20)
    """
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
    spacy.prefer_gpu()

    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        logger.info("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank("de")  # create blank Language class
        logger.info("Created blank 'de' model")

    # add the text classifier to the pipeline if it doesn't exist
    # nlp.create_pipe works for built-ins that are registered with spaCy
    if "textcat" not in nlp.pipe_names:
        textcat = nlp.create_pipe(
            "textcat", config={"exclusive_classes": True, "architecture": "ensemble"}
        )
        nlp.add_pipe(textcat, last=True)
    # otherwise, get it, so we can add labels to it
    else:
        textcat = nlp.get_pipe("textcat")

    logger.info("Loading data...")
    labels, (train_texts, train_cats), (dev_texts, dev_cats) = load_data(ds_path)

    for l in labels:
        textcat.add_label(l)

    logger.info(
        "Using ({} training, {} evaluation)".format(len(train_texts), len(dev_texts))
    )
    train_data = list(zip(train_texts, [{"cats": cats} for cats in train_cats]))
    # print(train_data[0])

    # get names of other pipes to disable them during training
    pipe_exceptions = ["textcat", "trf_wordpiecer", "trf_tok2vec"]
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe not in pipe_exceptions]
    times = []
    with nlp.disable_pipes(*other_pipes):  # only train textcat
        # optimizer = nlp.begin_training(device=0)
        optimizer = nlp.begin_training()
        # optimizer = nlp.begin_training(cpu_count=12)
        logger.info("Training the model...")
        batch_sizes = compounding(4.0, 32.0, 1.1)
        # batch_sizes = compounding(4.0, 32.0, 1.001)
        t_e = trange(n_iter, leave=False)
        for i in t_e:
            t_e.set_description(f"At Epoch {i}")
            losses = {}
            # batch up the examples using spaCy's minibatch
            random.shuffle(train_data)
            batches = minibatch(train_data, size=batch_sizes)
            st = time.time()

            # TRAIN
            t = tqdm(list(batches))
            for batch in t:
                t.set_description("Running Batches")
                texts, annotations = zip(*batch)
                nlp.update(texts, annotations, sgd=optimizer, drop=0.2, losses=losses)
            times.append(time.time() - st)

            # EVAL at last for perfomance reasons
            if i == n_iter - 1:
                with textcat.model.use_params(optimizer.averages):
                    write_out = i == n_iter - 1
                    sk_eval(
                        nlp.tokenizer,
                        textcat,
                        dev_texts,
                        dev_cats,
                        classes=labels,
                        write_out=write_out,
                        model_name=ds_path.parent.name,
                    )
                # evaluate on the dev data split off in load_data()
        with (Path("reports") / f"{ds_path.parent.name}_rep.txt").open("a") as f:
            f.write(f"TOTAL TRAIN TIME: {sum(times)}")

    if output_dir is not None:
        with nlp.use_params(optimizer.averages):
            nlp.to_disk(output_dir)
        logger.info("Saved model to %s", output_dir)

def load_data(ds_path: Path, split=0.8):
    """Loads all data from a given folder containing one or multiple csv files
    with columns "text";"label". It is preferred to name the parent folder like
    the dataset to ensure corect logging (such as agb/csv)

    Parameters
    ----------
    ds_path : pathlib.Path
        The folder path which contains one or more csvs with the needed columns
        for training and validation.
    split : float, optional
        Percentage amount of data to use for training (default = 0.8)

    Returns
    -------
    list[str]
        All of the labels that where found during processing
    (list[str], list[dict[str, int]])
        All training texts and labels in spacys format
    (list[str], list[dict[str, int]])
        All validation texts and labels in spacys format
    """
    dfs = [pd.read_csv(f, sep=";") for f in ds_path.glob("*.csv")]

    df = pd.concat(dfs)
    df["text"] = df["text"].astype(str)
    df = df[df["text"] != ""]
    df["label"] = df["label"].astype(str)
    logger.debug("Loaded a total of %d samples", len(df.index))

    labels = df.label.unique()
    logger.debug("Found Labels are")
    logger.debug(labels)

    texts, cats = [], []
    for i, items in df.iterrows():
        texts.append(items.text)
        cats.append({k: k == items.label for k in labels})

    train_data = list(zip(texts, cats))
    random.shuffle(train_data)
    texts, cats = zip(*train_data)
    split = int(len(train_data) * split)
    return labels, (texts[:split], cats[:split]), (texts[split:], cats[split:])


def sk_eval(
    tokenizer, textcat, texts, cats, write_out=False, classes=[],
    model_name="bababooey"
):
    """Evaluates the textcat component of a pipline and optionally writes out
    the results (a classification report and a confusion matrix)

    Parameters
    ----------
    tokenizer : spacy.tokenizers.Tokenizer
        The spacy tokenizer of the pipeline that is being used during training
    textcat : spacy.pipeline.TextCategorzier
        The spacy TextCategorizer that has been trained and is part of the
        pipeline that needs to be evaluated
    texts : list[str]
        The validation texts that have been loaded by load_data
    cats : list[dict[str, int]]
        The categories in spacys needed format, loaded by load_data
    write_out : bool, optional
        Whether to write the results to disk for later viewing (default =
        False). Creates the folders ``reports`` and ``figs`` in the current
        working directory
    classes : list[str], optional
        All labels that are used during training (default = [])
    model_name : str, optional
        The models dataset type used when persisting the reports (default =
        bababooey)
    """
    def get_highest(cats):
        h_l, h = "", 0
        for k, v in cats.items():
            if v > h:
                h = v
                h_l = k
        return h_l

    docs = (tokenizer(text) for text in texts)
    y_test, y_pred = [], []
    eval_time = time.time()
    for i, doc in enumerate(textcat.pipe(docs)):
        gold = get_highest(cats[i])
        pred = get_highest(doc.cats)
        y_test.append(gold)
        y_pred.append(pred)
    eval_time = time.time() - eval_time
    report = metrics.classification_report(y_test, y_pred)
    logger.info("\n" + report)
    if write_out:
        sh.mkdir("-p", "reports")
        (Path("reports") / f"{model_name}_rep.txt").write_text(report)
        with (Path("reports") / f"{model_name}_rep.txt").open("a") as f:
            f.write(f"TOTAL EVAL TIME: {eval_time}")

        ## Plot confusion matrix
        cm = metrics.confusion_matrix(y_test, y_pred)
        df_cm = pd.DataFrame(cm, index=classes, columns=classes,)
        fig, ax = plt.subplots(figsize=(20, 20))
        heatmap = sns.heatmap(
            df_cm, annot=True, fmt="d", ax=ax, cmap=plt.cm.Blues_r, cbar=False
        )
        plt.title("Confusion matrix of the classifier")
        heatmap.yaxis.set_ticklabels(
            heatmap.yaxis.get_ticklabels(), rotation=0, ha="right", fontsize=14
        )
        heatmap.xaxis.set_ticklabels(
            heatmap.xaxis.get_ticklabels(), rotation=45, ha="right", fontsize=14
        )
        ax.set(xlabel="Pred", ylabel="True")
        # ax.set(xlabel="Pred", ylabel="True", xticklabels=classes,
        #       yticklabels=classes, title="Confusion matrix")
        plt.yticks(rotation=0)
        sh.mkdir("-p", "figs")
        plt.savefig(Path("figs") / f"{model_name}_cm.png")

if __name__ == "__main__":
    plac.call(main)
